#!/bin/bash
chroot-tool create
chroot-tool install acl attr authconfig bc bind-utils bzip2 \
                         cyrus-sasl-plain lsof libcgroup quota rhel-instnum \
                         cpuspeed dos2unix m2crypto sssd nc prctl redhat-lsb \
                         setarch time tree unix2dos unzip wget which zip zlib
chroot /chroot/sl5 bash
