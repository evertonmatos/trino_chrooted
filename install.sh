echo "CONFIGURANDO PARAMETROS"
LOCAL=`pwd`
CHROOT=/tmp/chroot
DNS=8.8.8.8
CENTOS=centos-release-6-9.el6.12.3.i686.rpm

echo "DESMONTANDO E LIMPANDO CHROOT ANTERIOR"
umount $CHROOT/sys $CHROOT/proc $CHROOT/dev
rm -rf $CHROOT/*

echo "RECRIANDO E MONTANDO ESTRUTURA"
mkdir -p $CHROOT/rpm $CHROOT/var $CHROOT/lib $CHROOT/sys $CHROOT/proc $CHROOT/dev $CHROOT/etc $CHROOT/root 
mount -o bind /sys $CHROOT/sys
mount -o bind /proc $CHROOT/proc
mount -o bind /dev $CHROOT/dev

cp -a $LOCAL/centos-release/$CENTOS $CHROOT/.

rpm --root $CHROOT --initdb
echo "nameserver $DNS" > $CHROOT/etc/resolv.conf

rpm --root $CHROOT -ivh --nodeps centos-release/$CENTOS
cp -a /etc/skel/.bashrc $CHROOT/root/.

echo "Deseja instalar o sistema de gerenciamento de arquivos? (s/n)"
read arquivos

if [ $arquivos  = "s" ];then
echo "INSTALANDO SISTEMA DE ARQUIVOS"
	yum --installroot=$CHROOT -y install coreutils
else
	echo "Coreutils não será instalado"
fi


#yum -c $LOCAL/etc/yum.conf --installroot /opt/chroot/ -y install centos-release/$CENTOS
